import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import Helpers

namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
               'dcterms' : '{http://purl.org/dc/terms/}',
                'mam' : '{http://www.vizrt.com/2010/mam}',
                'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
                'vaext' : '{http://www.vizrt.com/atom-ext}',
                'vdf' : '{http://www.vizrt.com/types}',
                'playout' : '{http://ns.vizrt.com/ardome/playout}' }

# prendi il numero di variante

def Prendi_Variant( id ):

	link = 'http://10.72.10.29/rest/digitalItem/variants/' + str(id) 
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)   
	result = urllib2.urlopen(request)
	varia = result.read()
	print varia
	variantID =  varia.split('<diactus:Item>')[-1].split('</diactus:Item>')[0]
	variantID = variantID.split('<diactus:Id>')[-1].split('</diactus:Id>')[0]
	#print variantID
	print ' Trovo il VarianID dell asset 820 : %s' % variantID
	return variantID


# con questa mi calcolo la differenza dal fuso di Greenwitch
# e la sbatto nella globalona delta che sommo all'inizio agli
# start delle liste e poi lavoro sempre in naive mode
delta = Helpers.CalcDeltaTimeUtc()

# prende lo stato del rubinetto
stato_rubinetto = Helpers.PrendiStatoRubinetto()

print stato_rubinetto

begintime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 8, 00, 00, 0) 
#print begintime
endtime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 23, 00, 00, 0) 

due = datetime.timedelta( seconds=120 )
time = datetime.datetime.now() - delta + due 
#time = datetime.datetime( 2014,07,12, 19, 00, 00, 0) 
#time = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 21, 05, 01, 0) 

print ' Actual TIME %s ' % time

# faccio il primo controllo se sono fuori dai limiti 
# deve essere spento .. altrimnti lo spengo se per qualche 
# motivo e' ancora acceso.

#Helpers.CheckLimits(time, begintime, endtime )

base64string = base64.encodestring('%s:%s' % ('claudio.perucca@rsi.ch', 'SxMQDeir')).replace('\n', '')
base64string = base64.encodestring('%s:%s' % ('post@adactus.no', 'V1m2e3o4#')).replace('\n', '')

db_finestre = Helpers.PrendiDb( begintime )

print Prendi_Variant(820)
# adesso devo guardare come sono messo con il time
# attuale rispetto alle finestre 
sono_in_finestra = False

for value in db_finestre:
	#print value
	if value[0] <= time <= value[1] + due :
		print 'Sono in finestra aperta'
		#sono in una finestra chiusa
		# allora se il rubinetto e' ON devo spegnerlo
		sono_in_finestra = True
		break


if ( sono_in_finestra !=  stato_rubinetto ):
	# dobbiamo  switchare l interruttore
	if stato_rubinetto: 
		# se e' acceso lo spengo
		#while the command:
		# http://10.72.10.29/rest/digitalItem/withdraw/742?publishPoint=3
		#will turn off the live Stream for RSI La Uno

		#print result.read()
		#Helpers.Dump_Link( link, './RESOURCES/rubinetto_1', base64string)
		print ' Spengo il rubinetto '
		link = 'http://10.72.10.29/rest/digitalItem/withdraw/' + Prendi_Variant(820) + '?publishPoint=3'
		link = 'http://10.72.10.29/rest/digitalItem/withdraw/1753?publishPoint=3'
		request = urllib2.Request(link)
		request.add_header("Authorization", "Basic %s" % base64string)   
		result = urllib2.urlopen(request)
		print result.read()
		stato_rubinetto = False
		out = 'OFF'
	else:
		# se e' spento lo accendo
		# The Command:
		# http://10.72.10.27/rest/digitalItem/publish/742?publishPoint=3
		# will turn on the Live Stream for RSI La Uno
		# prendo le varianti del 820
		print ' Accendo il rubinetto '
		request = urllib2.Request('http://10.72.10.29/rest/digitalItem/publish/' + Prendi_Variant(820) + '?publishPoint=3')
		link = 'http://10.72.10.29/rest/digitalItem/withdraw/1753?publishPoint=3'
		request.add_header("Authorization", "Basic %s" % base64string)   
		result = urllib2.urlopen(request)
		print result
		stato_rubinetto = True
		out = 'ON'

	Helpers.ScriviStatoRubinetto( out ) 

			
			
		# altrimenti tutto bene


#veda_dict = ExtractRange(veda_dict, begintime, endtime)
#print '\n Veda : entry in Range # %d ' % len(veda_dict)
#sorted_dict_rows_louise = sorted(veda_dict.items(), key=lambda i: i[1][2])
#for key, value in sorted_dict_rows_louise:
	#print value


#lista_apri_chiudi = CreaListaApriChiudi(louise_dict, veda_dict)
#import datetime
#>>> nowutc = datetime.datetime.utcnow()
#>>> now = datetime.datetime.now()
#>>> delta = now - nowutc
#>>> print delta
#2:00:02.320133

#xml = minidom.parse(result)
#fout = codecs.open("./playlist_1", 'w', 'utf-8')
#fout.write( xml.toxml() )
#fout.close()

finestre_per_escenic = Helpers.PrendiDb (begintime)
time = datetime.datetime.now() - delta
#time = datetime.datetime( 2014,07,12, 19, 50, 01, 0)

# prendo il valore della FA finestra Attuale
fa = Helpers.PrendiFA()

[prossimaNuova, value] = Helpers.PrendiProssimaScadenza(finestre_per_escenic, time)
[prossimaFA, valueFA] = Helpers.PrendiProssimaScadenza(fa, time)

print ' prossima scadenza delle nuove: %s - %s' % (prossimaNuova , value)
print ' prossima scadenza in escenic: %s - %s' % (prossimaFA, valueFA)

if not( prossimaNuova is None):
        if  ( prossimaNuova != prossimaFA or value != valueFA ):
                Helpers.MandaProssimaFinestraEscenic(value, delta)


