import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'vdf' : '{http://www.vizrt.com/types}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	exit(0)

	return



def Dump_Rows ( link, filename ):

	print ' in Dump Rows '

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib2.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			request = urllib2.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib2.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			return lis.attrib['href']
	return None


def PrendiField( entry , rel):

	list =  entry.findall(namespaces['vdf'] + "field")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['name']
		if rel == lis.attrib['name']:
			return lis.find(namespaces['vdf'] + "value").text

	return None

# formatta lo starttime in un oggetto datetime
# comodo per eventuali confronti di data
def FormatDateTime( startTime, delta):
	#print startTime
	startDateTime = datetime.datetime.strptime( startTime, "%Y-%m-%dT%H:%M:%S.%fZ" )
	#print  startDateTime + delta

	return startDateTime + delta

# questa server per calcolare l'oggetto datetime di END
# prsso dallo start + duration
def CalcEndTime( startTime, duration):
	#print startTime , startTime + datetime.timedelta( seconds = int(duration))
	endTime = startTime + datetime.timedelta( milliseconds = int(duration))
	return endTime


def CalcDeltaTimeUtc():

	nowutc = datetime.datetime.utcnow()
	now = datetime.datetime.now()
	delta = now - nowutc
	#print delta
	
	deltahour = datetime.timedelta(hours=int(delta.seconds/3600))
	#2:00:02.320133
	#print deltahour

	return deltahour


def TrovaIndex( list, date ):
	
	# devo girare sulla lista finche' la mia date non e' compresa
	# tra la start e l'endtime
	print date 
	for key, value in sorted(list.items()):
		
		if  value[0] < date < value[2]:
			print ' Trovato Index '
			print value[0], value[2]
			print key,value
			print ' '
			return value
	print ' NON Trovato Index - restituisco None '
	return None

def TrovaNow( list ):
	
	now = datetime.datetime.now()

	# per questioni di testing 
	# DA RIMUOVERE 
	# now = datetime.datetime( 2014, 07, 23, 19, 00, 00, 0)
	
	return TrovaIndex( list, now )



# con uqesta mi calcolo la differenza dal fuso di Greenwitch
# e la sbatto nella globalona delta che sommo all'inizio agli
# start delle liste e poi lavoro sempre in naive mode
delta = CalcDeltaTimeUtc()
#print delta


def Prendi_Playlist_Date (date):

	#base64string = base64.encodestring('%s:%s' % ('perucca@rsi.ch', 'perucca')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('claudio.perucca', 'perucca1234')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'SxMQDeir')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'WaWA3pu')).replace('\n', '')

	#request = urllib2.Request("http://localhost/thirdparty/")
	#request.add_header("Authorization", "Basic %s" % base64string)
	#result = urllib2.urlopen(request)
	#xml = minidom.parse(result)
	#print xml.toxml()

	#strftime("%Y-%m-%d")
	after = date - datetime.timedelta(days=1)
	after = after.strftime("%Y-%m-%d")

	before = date + datetime.timedelta(days = 1)
	before = before.strftime("%Y-%m-%d")
	#print before, after
	
	request = urllib2.Request("http://10.72.10.11/thirdparty/playlist?start=0&num=100&after=" + after + "&before=" + before)


	#request.add_header("Authorization", "Basic %s" % base64string)
	#result = urllib2.urlopen(request)
	#xml = minidom.parse(result)
	#print xml.toxml()

	# rundown louise la1
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/2101406300000234841")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/2101406300000234841/rows")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/2101406290000234441/rows")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407140000242741&amp;start=0")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407140000242741&amp;start=51")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407130000242341&amp;start=51")

	# rundown veda la1
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/2101407140000242741/rows")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/2101407130000242341/rows")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/item/2101407150398301043")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407140000242741&amp;start=0")
	#request = urllib2.Request("http://10.72.10.11/thirdparty/playlist/item/2101407150398301043/metadata"thirdparty_e)

	#request = urllib2.Request("http://rsis-cs-maams1/thirdparty/")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist?start=1&num=100")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist/2101407200000251341")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist/2101407200000251341/rows")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407200000251341&amp;start=0")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407200000251341&amp;start=51")
	#request = urllib2.Request("http://10.72.10.10/thirdparty/playlist/row?num=50&amp;qPlaylist=2101407200000251341&amp;start=101")


	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)


	tree = ET.parse(result)



	# ne prendo la lista delle entry
	lista_entry = tree.findall( namespaces['atom'] + 'entry')
	print " prendo # %d run downs AFTER = %s BEFORE %s" % ( len(lista_entry), after, before)	
	return lista_entry




# con canale che puo essere La1 o La2
# e tipo chje puo essere Louise o Veda
def PrendiList( canale, tipo, lista_entry ):

	print '\n  Prendo le liste per %s - %s ' % (canale, tipo)
		
	count_entry = 0
	louise = {}
	id_param =  tipo.lower() + '_' + canale.lower() + '_'
	for entry in lista_entry:
		title =  entry.find( namespaces['atom']+ 'title').text 
		count_entry = count_entry + 1
		#print title
		id =  entry.find( namespaces['dcterms'] + 'identifier').text
		#print id
		#if '(Louise)' in title and 'La1' in title:
			#print entry.find( namespaces['atom']+ 'title').text
			#print entry.find( namespaces['playout'] + 'startTime').text
			#print entry.find( namespaces['playout'] + 'duration').text
			#louise[ title ] = [entry.find( namespaces['playout'] + 'startTime').text, 
					   #entry.find( namespaces['playout'] + 'duration').text,
					   #entry.find( namespaces['dcterms'] + 'identifier').text]
		if id_param  in id :
			#print entry.find( namespaces['atom']+ 'title').text
			#print entry.find( namespaces['playout'] + 'startTime').text
			#print entry.find( namespaces['playout'] + 'duration').text
			start = FormatDateTime( entry.find( namespaces['playout'] + 'startTime').text, delta)
			louise[ title ] = [start,
					   entry.find( namespaces['playout'] + 'duration').text,
					   CalcEndTime( start, float(entry.find( namespaces['playout'] + 'duration').text)*1000.0),
					   entry.find( namespaces['dcterms'] + 'identifier').text,
					   PrendiLink(entry, 'self')]

	return louise

def PrendiRows(entry_now_louise):
	

	print ' Ne Prendo le Rows \n'
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(entry_now_louise + '/rows')

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./rows_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)

	# ne prendo la lista delle entry
	lista_rows = tree.findall( namespaces['atom'] + 'entry')

	# ocio che potrebbe essere solo la prima parte
	# devo verificare che ci siano tutti altrimenti
	# devo girare sui next
	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			#e qui faccio la request sul campo next
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			lista_rows = lista_rows + tree.findall( namespaces['atom'] + 'entry')

	print ' totale Row prese = % d ' % len(lista_rows)
 
	return lista_rows

def GetMetadataLink( link ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	#request = urllib2.Request("http://10.72.10.11/thirdparty/asset/item/2101407090017465121")

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)

	#fout = codecs.open("./rows_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)



	# ne prendo la lista delle entry
	# asset = tree.find( namespaces['atom'] + 'entry')
	link =  PrendiLink(tree, 'describedby')
	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./meta_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()


	return link


def GetMetadata( link ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	#request = urllib2.Request("http://10.72.10.11/thirdparty/asset/item/2101407090017465121")
	#request = urllib2.Request("http://10.72.10.11/api/metadata/form/louise_version/r1")

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./version_1", 'a', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)



	# ne prendo la lista delle entry
	fields = tree.find( namespaces['vdf'] + 'field')
	geolimited =  PrendiField(tree, 'srgrights.geolimited')
	startbroadcast =  PrendiField(tree, 'srgbroadcast.startbroadcast')
	endbroadcast =  PrendiField(tree, 'srgbroadcast.endbroadcast')
	#link = 'http://10.72.10.11/api/asset/item/2101407090017466221/metadata'
	#request = urllib2.Request(link)

	#request.add_header("Authorization", "Basic %s" % base64string)
	#result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./tg_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()


	return [geolimited, startbroadcast, endbroadcast]


def ParsaRows( lista_entry ):
		
	count_entry = 0
	rows = {}
	print ' Inizia Parsa Rows ... '

	for entry in lista_entry:
		title =  entry.find( namespaces['atom']+ 'title').text 
		if '-- Fine --' in title:
			break
		id  =  entry.find( namespaces['atom']+ 'id').text 
		count_entry = count_entry + 1
		print title
		#if  not  'L\'ESTASI E L\'AGONIA' in title:
			#continue
		feed_entry = entry.find(  namespaces['atom'] + 'content' + '/' + namespaces['atom']+ 'feed' + '/' + namespaces['atom'] + 'entry')
		title2 =  feed_entry.find( namespaces['atom']+ 'title').text
		duration =  feed_entry.find( namespaces['vaext']+ 'duration')
		#print duration.attrib['framecount']
		#print duration.attrib['framerate']
		#print duration.text
		startOffset =  feed_entry.find( namespaces['playout']+ 'startOffset').text
		#print startOffset

		metalink = feed_entry.find(namespaces['mam']+ 'asset')
		#ET.dump(metalink)
		#print metalink.attrib['href']

		startTime = FormatDateTime( entry.find( namespaces['playout']+ 'startTime').text , delta )
		#print startTime

		# 0 title 1
		# 1 title 2
		# 2 starttime
		# 3 duration
		# 4 endtime
		# 5 geolinited
		# 6 href a i metadata
		# 7 start offset
		# 8 vme asset id
		# 9 startbroadcast
		# 10 endvroadcast 
		
		if not metalink is None:
			link_diritti = GetMetadataLink( metalink.attrib['href'] )
			asset_id = metalink.attrib['ref']
			[geolimited,  startbroadcast, endbroadcast] = GetMetadata( link_diritti )
			print startbroadcast, endbroadcast
		else:
			link_diritti = None
			geolimited = True
			startbroadcast = None
			endbroadcast = None
			asset_id = None
		
		#rows[ id ] = [title, title2,startTime, duration.text,  float(duration.text)*1000.0, link_diritti , startOffset ]
		rows[ id ] = [title, title2,startTime, duration.text, CalcEndTime(startTime, float(duration.text)*1000.0 ), geolimited, link_diritti , startOffset , asset_id, startbroadcast, endbroadcast]
		#GetMetadata( metalink.attrib['href'])
		#exit(0)

	print ' Finisce Parsa Rows ... '
	return rows

def TogliDiritti( dict_entry ):

	lista_finestre = []

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	count = 0
	Senza_Diritti = False
	for key, value in sorted_dict:
		print key, value
		if value[5] is None: 
			# il 5 est None significa che non abbiamo i diritti
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti mode allora allungo la finestra
				print ' allungo la finestra '
				date = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				
				lista_finestre[ count ] = [ lista_finestre[count][0] , date+ delta ]
				
			else :
				# altrimenti inizio una finestra chiusa finche dura'
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				end = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				print 'inizio finestra chiusa a %s ' % (date + delta)
				lista_finestre.append(  [ date + delta, end + delta ] )
			Senza_Diritti = True
			
		else:
			#se invece la entry ha i diritti
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti  mode allora chiudo la 
				# finestra di chiusura che inizia uno spazio buono
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				print ' chiudo la finestra a %s ' % (date+ delta)
				
				lista_finestre[ count ] = [ lista_finestre[count][0] , date+ delta ]
				count = count + 1

				
			else :
				# altrimenti allungo lo spazio buono
				print ' allungo lo spazio buono '
			Senza_Diritti = False
			

	return lista_finestre



def ExtractRange( dict_entry, begintime, endtime):

	dict_estratto = {}

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		if begintime <= value[2] <= endtime or begintime <= value[4] <= endtime:
			dict_estratto[key] = value
			#print key, value
	

	return dict_estratto

def GetLouiseVeda():

	veda = {}
	louise = {}

	# da ripristinare dopo test
	lista_entry = Prendi_Playlist_Date(datetime.datetime.utcnow())
	#lista_entry = Prendi_Playlist_Date(datetime.datetime(2014, 07, 23))
	print len(lista_entry)

	veda_list = PrendiList( 'La1' , 'Veda', lista_entry )

	count = 0 
	for key, value in sorted(veda_list.items()):
		print key, value
		count = count + 1
	print 'Presa list veda: trovate # ' , count

	entry_now_veda = TrovaNow(veda_list)
	
	if not entry_now_veda is None:
		# nel quarto campo c'e' l'href
		lista_rows = PrendiRows(entry_now_veda[4])
		veda = ParsaRows( lista_rows )



	louise_list = PrendiList( 'La1' , 'Louise', lista_entry )
	count = 0 
	for key, value in sorted(louise_list.items()):
		print key, value
		count = count + 1
	print 'Presa list louise: trovate # ' , count

	entry_now_louise = TrovaNow(louise_list)

	# nel quarto campo c'e' l'href
	lista_rows = PrendiRows(entry_now_louise[4])
	print str(len(lista_rows))


	louise = ParsaRows( lista_rows )

	return [louise, veda]

#come date voglio una datetime nella froma datetime.datetime(2014, 07, 23))
#piuttosto che il classico datetime.datetime.utcnow())
def GetLouise(date):

	louise = {}

	# da ripristinare dopo test
	#lista_entry = Prendi_Playlist_Date(datetime.datetime.utcnow())
	lista_entry = Prendi_Playlist_Date(date)
	print len(lista_entry)

	louise_list = PrendiList( 'La1' , 'Louise', lista_entry )
	count = 0 
	#for key, value in sorted(louise_list.items()):
		#print key, value
		#count = count + 1
	print 'Presa list louise: trovate # ' , len(louise_list)

	entry_now_louise = TrovaIndex(louise_list, date)

	# nel quarto campo c'e' l'href
	lista_rows = PrendiRows(entry_now_louise[4])
	#print str(len(lista_rows))


	louise = ParsaRows( lista_rows )

	return louise

def  CreaRelazionrVedaLouise(louise, veda):
	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		print value[0], value[8]
		print ' trovato - %s - in louise con id : %s ' % ( value[0], value[8] )
		print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])
		#e lo cerco nelle entry di veda
		for key_veda, value_veda in sorted_dict_veda:
			if not (value_veda[8] is None) and value[8] in value_veda[8]:
				print ' trovato in veda match %s ' % ( value_veda[0] )
				print ' comincia alle : %s e finisce alle: %s ' % (value_veda[2], value_veda[4])
				print ' comincia alle : %s e finisce alle: %s ' % (value_veda[9], value_veda[10])
	return lista_ac

def  CreaListaApriChiudi(louise, veda):

	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		print value[0], value[8]
		print ' trovato - %s - in louise con id : %s ' % ( value[0], value[8] )
		print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])

	print '\n\n'
	for key, value in sorted_dict:
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])


	return lista_ac

def TrimmaFinestre(lista, begintime, endtime):

	# sistema i limiti estremi
	for lis in lista:
		if lis[0] < begintime: lis[0] = begintime
		if lis[1] > endtime : lis[1] = endtime
	return lista


def MandaProssimaFinestraEscenic(lista):


        # calcolo i valori di start e end dalla lista
        lista[0] = lista[0] - delta
        lista[1] = lista[1] - delta
        #modifica da Paolo del 10-09-2014 per inserire anche eventtime
        #e per mettere event a -2 dalla partenza
        #e start a -4 dalla partenza
        due = datetime.timedelta( seconds=120 )
        event = lista[0] - due
        lista[0] = event - due
        start = lista[0].strftime("%Y-%m-%dT%H:%M:%SZ")
        end = lista[1].strftime("%Y-%m-%dT%H:%M:%SZ")
        event = event.strftime("%Y-%m-%dT%H:%M:%SZ")

	print 'mando a escenic start %s - end %s ' % ( start, end)
	# devo mandar la roba ad escenic
	to_escenic_import_dir = '/var/escenic/import/default/tvsvizzera'
	#to_escenic_import_dir = '/tmp'
	# posso farlo con la curl oppure 
	# alla solita maniera con la import
	
	#fout = codecs.open( WorkDir + '/_clad_tmp_', 'w', 'utf-8')
	fout = codecs.open( '/home/perucccl/pyStream/_clad_tmp_', 'w', 'utf-8')
	# prendo il temnplate
	#nometemplate = Template_dir + '/Resource_Template.xml'
	nometemplate =  '/home/perucccl/pyStream/Resource_Template.xml'
        ftempl = codecs.open( nometemplate, 'r', 'utf-8')
	for line in ftempl:
		# nel mentre che lo scrivo fuori 
		# gli cambio XX_START_XX con lista[0]
		# e XX_END_XX con lista[1]
		fout.write(line.replace('XX_START_XX' , start).replace('XX_END_XX', end).replace('XX_EVENT_XX', event))

	ftempl.close()
	fout.close()

	# quindi lo muovo nella diretory di import 
	# di escenic
	#shutil.copy(WorkDir + '/_clad_tmp_', to_escenic_import_dir)
	shutil.copy('/home/perucccl/pyStream/_clad_tmp_', to_escenic_import_dir + '/stream_update.xml')
	#gli cambio le permissions cosi la import 
	#puo leggerlo senza problemi 
	st = os.stat( to_escenic_import_dir + '/stream_update.xml')
	os.chmod( to_escenic_import_dir + '/stream_update.xml', st.st_mode | stat.S_IROTH | stat.S_IWOTH)
	
	
	#os.remove(WorkDir + '/_clad_tmp_')
	os.remove('/home/perucccl/pyStream/_clad_tmp_')


		
def Negate_Finestre( lista_f_c, begintime, endtime ):
	
	lista_fa = []
	
	#print lista_f_c[0][0]
	lista_fa.append([ begintime, endtime]) 

	count = 0
	for lin in lista_f_c:
		print lin
		lista_fa[count] = [ lista_fa[count][0], lin[0] ]
		lista_fa.append( [ lin[1], lin[1]]  )
		print lista_fa
		count = count + 1
		
	lista_fa[len(lista_fa)-1] = [ lista_fa[len(lista_fa)-1][0], endtime ]

	print ' \nfinale ' 
	print lista_fa
	
	result_fa = []

	for fa in lista_fa:
		print fa
		if not fa[0] == fa[1]:
			result_fa.append(fa)
	print ' \nfinale trimmata' 
	print result_fa
	print '\n'
			
	return result_fa	
		
def PrendiProssimaScadenza(lista, time):

		
	for value in lista:
		#print value
		if time < value[0] :
			return [ value[0], value ]
		if time < value[1] :
			return [ value[1], value ]
	 
	return[ None, None ]

#Dump_Link("http://10.72.10.11/thirdparty/playlist/2101407220000253541", 'lista_veda')
#Dump_Rows("http://10.72.10.11/thirdparty/playlist/2101407220000253541/rows", 'lista_veda_rows')
#Dump_Rows("http://10.72.10.11/thirdparty/playlist/2101407080000239541/rows", 'lista_louise_rows')




# nei due time di range devo buttare dentro 
# il giorno attuale e poi il range dalle 19.00 alle 22.00
begintime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 8, 00, 00, 0) 
#print begintime
endtime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 23, 00, 00, 0) 
#print endtime
#begintime = datetime.datetime( 2014,07,12, 19, 00, 00, 0) 
print ' begin Time = %s ' % begintime
#endtime = datetime.datetime( 2014,07,12, 22, 00, 00, 0) 
print ' end Time = %s ' % endtime

#[louise_dict, veda_dict] = GetLouiseVeda()
louise_dict = GetLouise( begintime)
louise_dict = ExtractRange(louise_dict, begintime, endtime)
print ' \nLouise : entry in Range # %d ' % len(louise_dict)
louise_dict = TogliDiritti( louise_dict )
print ' Louise : finestre SENZA diritti  # %d ' % len(louise_dict)
louise_dict = TrimmaFinestre(louise_dict, begintime, endtime)
for lis in louise_dict:
	print lis

finestre_per_escenic = Negate_Finestre( louise_dict, begintime, endtime )
for lis in finestre_per_escenic:
	print lis

db_finestre = Helpers.PrendiDb (begintime)
# se mi ha tornato None significa che per oggi nessuno
# ha ancora fatto il Db continuiamo tranquilli

if not db_finestre is None:
	print ' esisteva db '

# adesso posso guardare le cose che sono cambiate
# e scrivere il log per Paolo .. TODO

# adesso prima di riscriverlo fuori 
# in base a tempo e prossima apertura/chiusura
# dobbiamo mandare le giuste cose a escenic

time = datetime.datetime.now()
#time = datetime.datetime( 2014,07,12, 19, 50, 01, 0) 

# prendo il valore della FA finestra Attuale
fa = Helpers.PrendiFA()

[prossimaNuova, value] = PrendiProssimaScadenza(finestre_per_escenic, time)
[prossimaFA, valueFA] = PrendiProssimaScadenza(fa, time)

print ' prossima scadenza delle nuove: %s - %s' % (prossimaNuova, value)
print ' prossima scadenza in escenic: %s - %s' % (prossimaFA, valueFA)

if not( prossimaNuova is None): 
	if  ( prossimaNuova != prossimaFA or value != valueFA ):
		MandaProssimaFinestraEscenic(value)

	Helpers.ScriviFA( value[0], value[1] )

Helpers.ScriviDb( finestre_per_escenic, begintime )



