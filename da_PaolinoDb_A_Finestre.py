import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil


base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

def PrendiField( entry , rel):

	list =  entry.findall(Helpers.namespaces['vdf'] + "field")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['name']
		if rel == lis.attrib['name']:
			return lis.find(Helpers.namespaces['vdf'] + "value").text

	return None

# formatta lo starttime in un oggetto datetime
# comodo per eventuali confronti di data
def FormatDateTime( startTime, delta):
	#print startTime
	startDateTime = datetime.datetime.strptime( startTime, "%Y-%m-%dT%H:%M:%S.%fZ" )
	#print  startDateTime + delta

	return startDateTime + datetime.timedelta(days=0)

# questa serve per calcolare l'oggetto datetime di END
# prsso dallo start + duration
def CalcEndTime( startTime, duration):
	#print startTime , startTime + datetime.timedelta( seconds = int(duration))
	endTime = startTime + datetime.timedelta( milliseconds = int(duration))
	return endTime


# con questa mi calcolo la differenza dal fuso di Greenwitch
# e la sbatto nella globalona delta che sommo all'inizio agli
# start delle liste e poi lavoro sempre in naive mode
delta = Helpers.CalcDeltaTimeUtc()
#print delta

def TogliDiritti( dict_entry ):

	print 'Togli Diritti'
	lista_finestre_senza_diritti = []

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	count = 0
	Senza_Diritti = False

	# 0 title 1
	# 1 title 2
	# 2 starttime
	# 3 duration
	# 4 endtime
	# 5 geolimited
	# 6 href a i metadata
	# 7 start offset
	# 8 vme asset id
	# 9 startbroadcast
	# 10 endvroadcast 
	
	for key, value in sorted_dict:
		print value[2], value[4], value[5]
		if not value[5]: 
			# il 5 est True significa che non abbiamo i diritti
			# o perche i metadati non ci sono oppure perche il geolimited e true
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti mode allora allungo la finestra
				print ' allungo la finestra '
				date = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				
				#lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date + delta ]
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date + datetime.timedelta(days=0) ]
				
			else :
				# altrimenti inizio una finestra chiusa finche dura'
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				end = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				print 'inizio finestra chiusa a %s ' % (date + datetime.timedelta(days=0))
				#lista_finestre_senza_diritti.append(  [ date + delta, end + delta ] )
				lista_finestre_senza_diritti.append(  [ date + datetime.timedelta(days=0), end + datetime.timedelta(days=0) ] )
			Senza_Diritti = True
			
		else:
			#se invece la entry ha i diritti
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti  mode allora chiudo la 
				# finestra di chiusura che inizia uno spazio buono
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				print ' fine la finestra chiusa a %s ' % (date+ datetime.timedelta(days=0))
				
				#lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ delta ]
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ datetime.timedelta(days=0) ]
				count = count + 1

				
			else :
				# altrimenti allungo lo spazio buono
				print ' allungo lo spazio buono '
			Senza_Diritti = False
			

	return lista_finestre_senza_diritti

def ExtractRange( dict_entry, begintime, endtime):

	dict_estratto = {}

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		if begintime <= value[2] <= endtime or begintime <= value[4] <= endtime:
			dict_estratto[key] = value
			#print key, value
	

	return dict_estratto

def  CreaListaApriChiudi(louise, veda):

	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		print value[0], value[8]
		print ' trovato - %s - in louise con id : %s ' % ( value[0], value[8] )
		print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])

	print '\n\n'
	for key, value in sorted_dict:
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])


	return lista_ac

def Prendi_Begintime( dict_in ):
	sorted_dict = sorted(dict_in.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		return value[2]
	return 'None'

def Merge_Negative( lista_in , begintime):

	
	print ' Merge _Negative '
	s1 = begintime - datetime.timedelta(seconds=1)
	domani = datetime.datetime.now() + datetime.timedelta(days=1)
	s2  = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0) -delta
	filter = [[s1,s2]]
	print 'Filter : '
	print filter
	# eventuale lista di test
	#m1 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 05, 35, 00, 0) - delta 
	#m2 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 12, 30, 00, 0) - delta
#
	#m3 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 12, 42, 00, 0) - delta
	#m4 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 19, 00, 00, 0) - delta
#
	#m5 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 20, 39, 00, 0) - delta 
	#m6 = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0) -delta

#
	#m7 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 16, 07, 00, 0) - delta
	#m8 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 17, 10, 00, 0) - delta
	#l1  = [[m1,m2], [m3,m4], [m5,m6] ]


	#times = lista_in + filter
	times = lista_in 
	#times = l1 + filter

	lista_result = Merge( sorted(times) )

	return lista_result

def Merge( times ):
	
	print ' Times 1'
	for tim in times:
		print tim
	print ' Times 2'
	saved = [times[0]]
	print ' Saved '
	print saved
	print ' Saved '
	for st, end in times[1:]:
		print saved[-1]
		print st, end
		a = saved[-1][0]
		print a
		b = saved[-1][1]
		print b
		
		if st <= b < end:
			saved[-1] = a,end
		elif b<st<=end:
			saved.append([st,end])
		else:
			pass
		print ' Saved '
		print saved
		print ' Saved '
	return saved

def TrimmaFinestre(lista, begintime, endtime):

	print ' TrimmaFinestre '
	# entrano finestre negative
	print begintime
	print endtime
	# sistema i limiti estremi
	for lis in lista:
		print lis[0], lis[1]
		if lis[0] < begintime: 
			#print  str(lis[0]) + ' minore di ' + str(begintime)
			lis[0] = begintime
		if lis[1] > endtime : 
			#print  str(lis[1]) + ' maggiore di ' + str(endtime)
			lis[1] = endtime
	
	# aggiunta 02 10 2014 per permettere l inserimento dei limiti
	# editoriali dalle 12:25 alle 12:50 
	# e dalle 18_55 alle 20_40
	# che corrispondono a due finestre chiuse dalle 12:00 alle 12:25
	# e dalle 12:50 alle 18:55
	
	for lin in lista:
		print lin

	print 'Fine  TrimmaFinestre '
	lista = Merge_Negative( lista, begintime )
	
	return lista


def Negate_Finestre( lista_f_c, begintime, endtime ):
	
	print '\n Inizia Negate Finestre ...\n '
	# lista finestre aperte per adesso vuota
	lista_fa = []
	
	#print lista_f_c[0][0]
	# la riempio con la finestra totale di apertura
	# da begin time a endtime
	lista_fa.append([ begintime, endtime]) 

	count = 0
	for lin in lista_f_c:
		print ' start %s - end %s ' % (lin[0], lin[1])
		lista_fa[count] = [ lista_fa[count][0], lin[0] ]
		lista_fa.append( [ lin[1], lin[1]]  )
		print lista_fa
		count = count + 1
		
	lista_fa[len(lista_fa)-1] = [ lista_fa[len(lista_fa)-1][0], endtime ]

	print ' \nfinale Finestre CON DIRITTI ' 
	#print lista_fa
	
	result_fa = []

	# toglie le finestre che hanno inizio == fine
	for fa in lista_fa:
		print ' start %s - end %s ' % (fa[0], fa[1])
		if not fa[0] == fa[1]:
			result_fa.append(fa)
	print ' \nfinale  Finestre CON DIRITTI trimmata in orario nostro:' 
	for fa in result_fa:
		print ' start %s - end %s ' % (fa[0] + delta, fa[1] + delta)
	print '\n'
	print '\n Fine Negate Finestre ...\n '
	print ' \nfinale  Finestre CON DIRITTI trimmata senza delta:' 
	for fa in result_fa:
		print ' start %s - end %s ' % (fa[0], fa[1])
	print '\n'
	print '\n Fine Negate Finestre ...\n '
			
	return result_fa	

def Paolino_2_Dict(paolino_db, time):


	print ' Paolino_2_Dict'
	result_dict = {}

	for idx, lis in enumerate(paolino_db):
		lis =  lis.split('_!_')
		print lis
		#start = datetime.datetime( time.year, time.month, time.day, int(lis[1].split(':')[0]), int(lis[1].split(':')[1]),int(lis[1].split(':')[-1]),0)
		start =  datetime.datetime.strptime( lis[1], "%Y-%m-%d %H:%M:%S" )
		startF = start.strftime("%Y-%m-%dT%H:%M:%SZ")
		end = datetime.datetime.strptime( lis[2], "%Y-%m-%d %H:%M:%S" )
		endF = end.strftime("%Y-%m-%dT%H:%M:%SZ")
		Off_uguale_False = False if 'Off' in str(lis[4]) else True
		result_dict[ lis[0] + '_' + str(idx) ] = [lis[0], lis[0] ,start, 'dummy', end, Off_uguale_False, 'dummy',  'dummy',  'dummy',  startF,  endF,   ]

	print ' Fine  Paolino_2_Dict'
	return result_dict

begintime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 01, 00, 00, 0)
begintime = begintime - delta
activate = begintime.strftime("%Y-%m-%d %H:%M:%S.0")

domani = datetime.datetime.now() + datetime.timedelta(days=1)

endtime = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0)
endtime = endtime - delta
expire = endtime.strftime("%Y-%m-%d %H:%M:%S.0")
print ' begin Time = %s ' % begintime
print ' end Time = %s ' % endtime


 
time = datetime.datetime.now()
#time = datetime.datetime( 2014,07,12, 19, 50, 01, 0) 

paolino_db = Helpers.PrendiPaolinoDb( time ) 

louise_dict = Paolino_2_Dict(paolino_db, time) 


#sorted_dict = sorted(louise_dict.items(), key=lambda i: i[1][2])
#for key, value in sorted_dict:
	#print key,value

begintime = Prendi_Begintime( louise_dict )

print ' NEW begin Time = %s ' % begintime

louise_dict = TogliDiritti( louise_dict )
print '\n  Louise : finestre SENZA diritti  # %d ' % len(louise_dict)

louise_dict = TrimmaFinestre(louise_dict, begintime, endtime)

print ' \n Finestre SENZA diritti : '
for lis in louise_dict:
	print ' start %s - end %s ' % (lis[0], lis[1])

finestre_per_escenic = Negate_Finestre( louise_dict, begintime, endtime )
for lis in finestre_per_escenic:
	print lis

db_finestre = Helpers.PrendiDb (begintime)
# se mi ha tornato None significa che per oggi nessuno
# ha ancora fatto il Db continuiamo tranquilli

if not db_finestre is None:
	print ' esisteva db '

# adesso posso guardare le cose che sono cambiate
# e scrivere il log per Paolo .. TODO

# adesso prima di riscriverlo fuori 
# in base a tempo e prossima apertura/chiusura
# dobbiamo mandare le giuste cose a escenic


# prendo il valore della FA finestra Attuale
fa = Helpers.PrendiFA()

[prossimaNuova, value] = Helpers.PrendiProssimaScadenza(finestre_per_escenic, time)
[prossimaFA, valueFA] = Helpers.PrendiProssimaScadenza(fa, time)

print ' prossima scadenza delle nuove: %s - %s' % (prossimaNuova, value)
print ' prossima scadenza in escenic: %s - %s' % (prossimaFA, valueFA)

if not( prossimaNuova is None): 
	if  ( prossimaNuova != prossimaFA or value != valueFA ):
		print ' Helpers.MandaProssimaFinestraEscenic'
		print 'Value : ' + str(value)
		print prossimaNuova
		print prossimaFA
		print ' valueFA : ' + str(valueFA)
		Helpers.MandaProssimaFinestraEscenic(value, datetime.timedelta(days=0), activate, expire)

	#print 'Value sotto: ' + str(value)
	Helpers.ScriviFA( value[0], value[1] )

Helpers.ScriviDb( finestre_per_escenic, begintime )



