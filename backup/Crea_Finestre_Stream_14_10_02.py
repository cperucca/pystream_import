import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil


base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')


# cerca tra i link dell atom e trova il link con attributo == rel
def PrendiLink( entry , rel):

	list =  entry.findall(Helpers.namespaces['atom'] + "link")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			return lis.attrib['href']
	return None


def PrendiField( entry , rel):

	list =  entry.findall(Helpers.namespaces['vdf'] + "field")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['name']
		if rel == lis.attrib['name']:
			return lis.find(Helpers.namespaces['vdf'] + "value").text

	return None

# formatta lo starttime in un oggetto datetime
# comodo per eventuali confronti di data
def FormatDateTime( startTime, delta):
	#print startTime
	startDateTime = datetime.datetime.strptime( startTime, "%Y-%m-%dT%H:%M:%S.%fZ" )
	#print  startDateTime + delta

	return startDateTime + delta

# questa server per calcolare l'oggetto datetime di END
# prsso dallo start + duration
def CalcEndTime( startTime, duration):
	#print startTime , startTime + datetime.timedelta( seconds = int(duration))
	endTime = startTime + datetime.timedelta( milliseconds = int(duration))
	return endTime


def CalcDeltaTimeUtc():

	nowutc = datetime.datetime.utcnow()
	now = datetime.datetime.now()
	delta = now - nowutc
	#print delta
	
	deltahour = datetime.timedelta(hours=int(delta.seconds/3600))
	#2:00:02.320133
	#print deltahour

	return deltahour


def TrovaIndex( list, date ):
	
	# devo girare sulla lista finche' la mia date non e' compresa
	# tra la start e l'endtime
	print date 
	for key, value in sorted(list.items()):
		
		if  value[0] < date < value[2]:
			print ' Trovato Index '
			print value[0], value[2]
			print key,value
			print ' '
			return value
	print ' NON Trovato Index - restituisco None '
	return None

def TrovaNow( list ):
	
	now = datetime.datetime.now()

	# per questioni di testing 
	# DA RIMUOVERE 
	# now = datetime.datetime( 2014, 07, 23, 19, 00, 00, 0)
	
	return TrovaIndex( list, now )



# con uqesta mi calcolo la differenza dal fuso di Greenwitch
# e la sbatto nella globalona delta che sommo all'inizio agli
# start delle liste e poi lavoro sempre in naive mode
delta = CalcDeltaTimeUtc()
#print delta


def Prendi_Playlist_Date (date):


	# mi trovo il tempo dopo la data giusta
	after = date - datetime.timedelta(days=1)
	after = after.strftime("%Y-%m-%d")

	# mi trovo il tempo prima della data giusta
	before = date + datetime.timedelta(days = 1)
	before = before.strftime("%Y-%m-%d")
	#print before, after
	
	request = urllib2.Request("http://10.72.10.11/thirdparty/playlist?start=0&num=100&after=" + after + "&before=" + before)

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	tree = ET.parse(result)
	#ET.dump(tree)

	# ne prendo la lista delle entry
	lista_entry = tree.findall( Helpers.namespaces['atom'] + 'entry')
	print " prendo # %d - run downs AFTER = %s BEFORE %s" % ( len(lista_entry), after, before)	

	# restituisce in lista_entry la lista di TUTTI i rundown a partire
	# dal giorno prima fino al giorno dopo
	return lista_entry




# con canale che puo essere La1 o La2
# e tipo che puo essere Louise o Veda
# la lista_antry e' la lista di rundown creata da Prendi_Playlist_Date
def PrendiList( canale, tipo, lista_entry ):

	print '\n  Prendo le liste per %s - %s \n' % (canale, tipo)
		
	count_entry = 0
	louise = {}
	id_param =  tipo.lower() + '_' + canale.lower() + '_'
	for entry in lista_entry:
		title =  entry.find( Helpers.namespaces['atom']+ 'title').text 
		# del tipo  <ns0:title>La2 2014-10-01 (Veda) </ns0:title>
		count_entry = count_entry + 1
		#print title
		id =  entry.find( Helpers.namespaces['dcterms'] + 'identifier').text
		# del tipo <ns1:identifier xmlns:ns1="http://purl.org/dc/terms/">veda_la2_20141001</ns1:identifier>
		#print id
		if id_param  in id :
			#print entry.find( Helpers.namespaces['atom']+ 'title').text
			# del tipo  <ns0:title>La2 2014-10-01 (Veda) </ns0:title>
			#print entry.find( Helpers.namespaces['playout'] + 'startTime').text
			# del tipo <ns1:startTime xmlns:ns1="http://ns.vizrt.com/ardome/playout">2014-09-30T22:00:00.000Z</ns1:startTime>
			#print entry.find( Helpers.namespaces['playout'] + 'duration').text
			# del tipo <ns1:duration xmlns:ns1="http://ns.vizrt.com/ardome/playout">86400</ns1:duration>
			start = FormatDateTime( entry.find( Helpers.namespaces['playout'] + 'startTime').text, delta)
			# chiave = titolo
			# 0 = start
			# 1 = duration
			# 2 = endtime
			# 3 = identifier (Es:veda_la2_20141001, louise_la1_20141010 ... )
			# 4 = link a self restituito da PrendiLink
			louise[ title ] = [start,
					   entry.find( Helpers.namespaces['playout'] + 'duration').text,
					   CalcEndTime( start, float(entry.find( Helpers.namespaces['playout'] + 'duration').text)*1000.0),
					   entry.find( Helpers.namespaces['dcterms'] + 'identifier').text,
					   PrendiLink(entry, 'self')]

	return louise
# come parametro vuole il link a self di un rundown
def PrendiRows(entry_now_louise):
	

	print ' Ne Prendo le Rows \n'

	request = urllib2.Request(entry_now_louise + '/rows')

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./RESOURCES/rows_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)

	# qui ha preso un atom come RESOURCES/rows_1

	# ne prendo la lista delle entry
	lista_rows = tree.findall( Helpers.namespaces['atom'] + 'entry')

	# ocio che potrebbe essere solo la prima parte
	# devo verificare che ci siano tutti altrimenti
	# devo girare sui next
	totresult = int(tree.find( Helpers.namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( Helpers.namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			#e qui faccio la request sul campo next
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			lista_rows = lista_rows + tree.findall( Helpers.namespaces['atom'] + 'entry')

	print ' totale Row prese = % d ' % len(lista_rows)
 
	return lista_rows

def GetMetadataLink( link ):
		

	request = urllib2.Request(link)
	#request = urllib2.Request("http://10.72.10.11/thirdparty/asset/item/2101407090017465121")

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)

	#fout = codecs.open("./rows_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)



	# ne prendo la lista delle entry
	# asset = tree.find( Helpers.namespaces['atom'] + 'entry')
	link =  PrendiLink(tree, 'describedby')
	request = urllib2.Request(link)
	#print link

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./meta_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()


	return link


def GetMetadata( link ):
		

	request = urllib2.Request(link)
	#request = urllib2.Request("http://10.72.10.11/thirdparty/asset/item/2101407090017465121")
	#request = urllib2.Request("http://10.72.10.11/api/metadata/form/louise_version/r1")

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./version_1", 'a', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()

	tree = ET.parse(result)
	#ET.dump(tree)


	# ne prendo la lista delle entry
	fields = tree.find( Helpers.namespaces['vdf'] + 'field')
	
	geolimited =  PrendiField(tree, 'srgrights.geolimited')
	# metto un booleano a geolimited per segnare se True o False
	# se i metadati non ci sono oppure perche il geolimited e true
	# e uguale a False
	if geolimited == 'true' or geolimited is None:
		geolimited = True
	else:
		geolimited = False

	#print geolimited

	startbroadcast =  PrendiField(tree, 'srgbroadcast.startbroadcast')
	endbroadcast =  PrendiField(tree, 'srgbroadcast.endbroadcast')
	mediastart = PrendiField(tree, 'srg.mediaStartTs')
	#print mediastart
	#link = 'http://10.72.10.11/api/asset/item/2101407090017466221/metadata'
	#request = urllib2.Request(link)

	#request.add_header("Authorization", "Basic %s" % base64string)
	#result = urllib2.urlopen(request)

	#xml = minidom.parse(result)
	#fout = codecs.open("./tg_1", 'w', 'utf-8')
	#fout.write( xml.toxml() )
	#fout.close()


	return [geolimited, startbroadcast, endbroadcast, mediastart]


def ParsaVeda( lista_entry ):
		
	count_entry = 0
	rows = {}
	print '\n Inizia Parsa Veda ...\n '

	for entry in lista_entry:
		title =  entry.find( Helpers.namespaces['atom']+ 'title').text 
		if '-- Fine --' in title:
			break
		id  =  entry.find( Helpers.namespaces['atom']+ 'id').text 
		count_entry = count_entry + 1
		print title
		#if  not  'METEO REGIONALE' in title:
			#continue
		feed_entry = entry.find(  Helpers.namespaces['atom'] + 'content' + '/' + Helpers.namespaces['atom']+ 'feed' + '/' + Helpers.namespaces['atom'] + 'entry')
		#ET.dump(feed_entry)
		#exit(0)

		title2 =  feed_entry.find( Helpers.namespaces['atom']+ 'title').text
		duration =  feed_entry.find( Helpers.namespaces['vaext']+ 'duration')
		#print duration.attrib['framecount']
		#print duration.attrib['framerate']
		#print duration.text
		startOffset =  feed_entry.find( Helpers.namespaces['playout']+ 'startOffset').text
		#print startOffset

		metalink = feed_entry.find(Helpers.namespaces['mam']+ 'asset')
		#ET.dump(metalink)
		#print metalink.attrib['href']

		startTime = FormatDateTime( entry.find( Helpers.namespaces['playout']+ 'startTime').text , delta )
		#print startTime

		# 0 title 1
		# 1 title 2
		# 2 starttime
		# 3 duration
		# 4 endtime
		# 5 geolimited
		# 6 href a i metadata
		# 7 start offset
		# 8 vme asset id
		# 9 startbroadcast
		# 10 endvroadcast 
		
		if not metalink is None:
			link_diritti = GetMetadataLink( metalink.attrib['href'] )
			asset_id = metalink.attrib['ref']
			[geolimited,  startbroadcast, endbroadcast, mediastart] = GetMetadata( link_diritti )
		else:
			link_diritti = None
			geolimited = True
			startbroadcast = None
			endbroadcast = None
			asset_id = None
			mediastart = None


		print mediastart, startbroadcast, geolimited, endbroadcast
		
		rows[ id ] = [title, title2,startTime, duration.text, CalcEndTime(startTime, float(duration.text)*1000.0 ), geolimited, link_diritti , startOffset , asset_id, startbroadcast, endbroadcast]

	print ' Finisce Parsa Veda ... '
	return rows


def ParsaRows( lista_entry ):
		
	count_entry = 0
	rows = {}
	print '\n Inizia Parsa Rows ...\n '

	for entry in lista_entry:
		title =  entry.find( Helpers.namespaces['atom']+ 'title').text 
		if '-- Fine --' in title:
			break
		id  =  entry.find( Helpers.namespaces['atom']+ 'id').text 
		count_entry = count_entry + 1
		print title
		#if  not  'METEO REGIONALE' in title:
			#continue
		feed_entry = entry.find(  Helpers.namespaces['atom'] + 'content' + '/' + Helpers.namespaces['atom']+ 'feed' + '/' + Helpers.namespaces['atom'] + 'entry')
		#ET.dump(feed_entry)
		#exit(0)
		title2 =  feed_entry.find( Helpers.namespaces['atom']+ 'title').text
		duration =  feed_entry.find( Helpers.namespaces['vaext']+ 'duration')
		#print duration.attrib['framecount']
		#print duration.attrib['framerate']
		#print duration.text
		startOffset =  feed_entry.find( Helpers.namespaces['playout']+ 'startOffset').text
		#print startOffset

		metalink = feed_entry.find(Helpers.namespaces['mam']+ 'asset')
		#ET.dump(metalink)
		#print metalink.attrib['href']

		startTime = FormatDateTime( entry.find( Helpers.namespaces['playout']+ 'startTime').text , delta )
		#print startTime

		# 0 title 1
		# 1 title 2
		# 2 starttime
		# 3 duration
		# 4 endtime
		# 5 geolimited
		# 6 href a i metadata
		# 7 start offset
		# 8 vme asset id
		# 9 startbroadcast
		# 10 endvroadcast 
		
		if not metalink is None:
			link_diritti = GetMetadataLink( metalink.attrib['href'] )
			asset_id = metalink.attrib['ref']
			[geolimited,  startbroadcast, endbroadcast, mediastart] = GetMetadata( link_diritti )
		else:
			link_diritti = None
			geolimited = True
			startbroadcast = None
			endbroadcast = None
			asset_id = None
			mediastart = None


		print mediastart, startbroadcast, geolimited, endbroadcast
		
		#rows[ id ] = [title, title2,startTime, duration.text,  float(duration.text)*1000.0, link_diritti , startOffset ]
		rows[ id ] = [title, title2,startTime, duration.text, CalcEndTime(startTime, float(duration.text)*1000.0 ), geolimited, link_diritti , startOffset , asset_id, startbroadcast, endbroadcast]
		#GetMetadata( metalink.attrib['href'])
		#exit(0)

	print ' Finisce Parsa Rows ... '
	return rows

def TogliDiritti( dict_entry ):

	lista_finestre_senza_diritti = []

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	count = 0
	Senza_Diritti = False

		# 0 title 1
		# 1 title 2
		# 2 starttime
		# 3 duration
		# 4 endtime
		# 5 geolimited
		# 6 href a i metadata
		# 7 start offset
		# 8 vme asset id
		# 9 startbroadcast
		# 10 endvroadcast 
		
	for key, value in sorted_dict:
		print value[2], value[4], value[5]
		if value[5]: 
			# il 5 est True significa che non abbiamo i diritti
			# o perche i metadati non ci sono oppure perche il geolimited e true
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti mode allora allungo la finestra
				print ' allungo la finestra '
				date = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ delta ]
				
			else :
				# altrimenti inizio una finestra chiusa finche dura'
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				end = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				print 'inizio finestra chiusa a %s ' % (date + delta)
				lista_finestre_senza_diritti.append(  [ date + delta, end + delta ] )
			Senza_Diritti = True
			
		else:
			#se invece la entry ha i diritti
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti  mode allora chiudo la 
				# finestra di chiusura che inizia uno spazio buono
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				print ' fine la finestra chiusa a %s ' % (date+ delta)
				
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ delta ]
				count = count + 1

				
			else :
				# altrimenti allungo lo spazio buono
				print ' allungo lo spazio buono '
			Senza_Diritti = False
			

	return lista_finestre_senza_diritti



def ExtractRange( dict_entry, begintime, endtime):

	dict_estratto = {}

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		if begintime <= value[2] <= endtime or begintime <= value[4] <= endtime:
			dict_estratto[key] = value
			#print key, value
	

	return dict_estratto

# come date voglio una datetime nella froma datetime.datetime(2014, 07, 23))
# piuttosto che il classico datetime.datetime.utcnow())
def GetVeda(date):

	veda = {}

	# da ripristinare dopo test
	#lista_entry = Prendi_Playlist_Date(datetime.datetime.utcnow())
	
	# restituisce in lista_entry la lista di TUTTI i rundown a partire
	# dal giorno prima fino al giorno dopo
	lista_entry = Prendi_Playlist_Date(date)
	#print len(lista_entry)

	# essendo una prendi Louise usa la PrendiList su La1 di Louise
	veda_list = PrendiList( 'La1' , 'Veda', lista_entry )
	count = 0 
	#for key, value in sorted(veda_list.items()):
		#print key, value
		#count = count + 1
	print 'Prese le liste di veda: trovate # ' , len(veda_list)

	entry_now_veda = TrovaIndex(veda_list, date)

	# nel quarto campo c'e' l'href
	lista_rows = PrendiRows(entry_now_veda[4])
	#print str(len(lista_rows))


	veda = ParsaVeda( lista_rows )

	return veda


# come date voglio una datetime nella froma datetime.datetime(2014, 07, 23))
# piuttosto che il classico datetime.datetime.utcnow())
def GetLouise(date):

	louise = {}

	# da ripristinare dopo test
	#lista_entry = Prendi_Playlist_Date(datetime.datetime.utcnow())
	
	# restituisce in lista_entry la lista di TUTTI i rundown a partire
	# dal giorno prima fino al giorno dopo
	lista_entry = Prendi_Playlist_Date(date)
	#print len(lista_entry)

	# essendo una prendi Louise usa la PrendiList su La1 di Louise
	louise_list = PrendiList( 'La1' , 'Louise', lista_entry )
	count = 0 
	#for key, value in sorted(louise_list.items()):
		#print key, value
		#count = count + 1
	print 'Prese le liste di louise: trovate # ' , len(louise_list)

	entry_now_louise = TrovaIndex(louise_list, date)

	# nel quarto campo c'e' l'href
	lista_rows = PrendiRows(entry_now_louise[4])
	#print str(len(lista_rows))


	louise = ParsaRows( lista_rows )

	return louise

def  CreaRelazioneVedaLouise(louise, veda):
	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		print value[0], value[8]
		print ' parto da - %s - in louise con id : %s ' % ( value[0], value[8] )
		print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		#print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])
		#e lo cerco nelle entry di veda
		for key_veda, value_veda in sorted_dict_veda:
			if not (value_veda[8] is None) and value[8] in value_veda[8]:
				print ' trovato in veda match %s ' % ( value_veda[0] )
				print ' trovato in veda match %s ' % ( value_veda[0] )
				print ' comincia alle : %s e finisce alle: %s ' % (value_veda[2], value_veda[4])
				print ' comincia alle : %s e finisce alle: %s ' % (value_veda[9], value_veda[10])
				lista_ac[ key ]= key_veda
				break
	return lista_ac

def  CreaListaApriChiudi(louise, veda):

	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		print value[0], value[8]
		print ' trovato - %s - in louise con id : %s ' % ( value[0], value[8] )
		print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])

	print '\n\n'
	for key, value in sorted_dict:
		print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])


	return lista_ac

def TrimmaFinestre(lista, begintime, endtime):

	# sistema i limiti estremi
	for lis in lista:
		#print lis[0], lis[1]
		if lis[0] < begintime: lis[0] = begintime
		if lis[1] > endtime : lis[1] = endtime
	return lista


def Negate_Finestre( lista_f_c, begintime, endtime ):
	
	print '\n Inizia Negate Finestre ...\n '
	# lista finestre aperte per adesso vuota
	lista_fa = []
	
	#print lista_f_c[0][0]
	# la riempio con la finestra totale di apertura
	# da begin time a endtime
	lista_fa.append([ begintime, endtime]) 

	count = 0
	for lin in lista_f_c:
		#print ' start %s - end %s ' % (lin[0], lin[1])
		lista_fa[count] = [ lista_fa[count][0], lin[0] ]
		lista_fa.append( [ lin[1], lin[1]]  )
		#print lista_fa
		count = count + 1
		
	lista_fa[len(lista_fa)-1] = [ lista_fa[len(lista_fa)-1][0], endtime ]

	print ' \nfinale Finestre APERTE ' 
	#print lista_fa
	
	result_fa = []

	# toglie le finestre che hanno inizio == fine
	for fa in lista_fa:
		print ' start %s - end %s ' % (fa[0], fa[1])
		if not fa[0] == fa[1]:
			result_fa.append(fa)
	print ' \nfinale  Finestre APERTE trimmata' 
	for fa in result_fa:
		print ' start %s - end %s ' % (fa[0], fa[1])
	print '\n'
	print '\n Fine Negate Finestre ...\n '
			
	return result_fa	



#Helpers.Dump_Link("http://10.72.10.11/thirdparty/playlist/2101407220000253541", 'lista_veda', base64string)
#Helpers.Dump_Rows("http://10.72.10.11/thirdparty/playlist/2101407220000253541/rows", 'lista_veda_rows', base64string)
#Helpers.Dump_Rows("http://10.72.10.11/thirdparty/playlist/2101407080000239541/rows", 'lista_louise_rows', base64string)
#Helpers.Dump_Link("http://10.72.10.11/api/asset/item/2101409170022158021/metadata", './RESOURCES/meta_11', base64string)




# nei due time di range devo buttare dentro 
# il giorno attuale e poi il range dalle bagintime alle endtime
begintime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 12, 00, 00, 0) 
endtime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 20, 40, 00, 0) 
print ' begin Time = %s ' % begintime
print ' end Time = %s ' % endtime

# prendo le liste di Louise che stanno attorno al begintime
# + o - un giorno
louise_dict = GetLouise( begintime)
veda_dict = GetVeda( begintime )

per_cave =   CreaRelazioneVedaLouise(louise_dict, veda_dict)

for key,value in per_cave.iteritems():
	print key,value


# e mi ritorna un dict con
		# 0 title 1
		# 1 title 2
		# 2 starttime
		# 3 duration
		# 4 endtime
		# 5 geolimited
		# 6 href a i metadata
		# 7 start offset
		# 8 vme asset id
		# 9 startbroadcast
		# 10 endbroadcast 
	

#tolgo le entry fuori dal range begintime - endtime
louise_dict = ExtractRange(louise_dict, begintime, endtime)
print ' \nLouise : entry in Range # %d ' % len(louise_dict)


louise_dict = TogliDiritti( louise_dict )
print '\n  Louise : finestre SENZA diritti  # %d ' % len(louise_dict)

louise_dict = TrimmaFinestre(louise_dict, begintime, endtime)

print ' \n Finestre SENZA diritti : '
for lis in louise_dict:
	print ' start %s - end %s ' % (lis[0], lis[1])

finestre_per_escenic = Negate_Finestre( louise_dict, begintime, endtime )
for lis in finestre_per_escenic:
	print lis

db_finestre = Helpers.PrendiDb (begintime)
# se mi ha tornato None significa che per oggi nessuno
# ha ancora fatto il Db continuiamo tranquilli

if not db_finestre is None:
	print ' esisteva db '

# adesso posso guardare le cose che sono cambiate
# e scrivere il log per Paolo .. TODO

# adesso prima di riscriverlo fuori 
# in base a tempo e prossima apertura/chiusura
# dobbiamo mandare le giuste cose a escenic

time = datetime.datetime.now()
#time = datetime.datetime( 2014,07,12, 19, 50, 01, 0) 

# prendo il valore della FA finestra Attuale
fa = Helpers.PrendiFA()

[prossimaNuova, value] = Helpers.PrendiProssimaScadenza(finestre_per_escenic, time)
[prossimaFA, valueFA] = Helpers.PrendiProssimaScadenza(fa, time)

print ' prossima scadenza delle nuove: %s - %s' % (prossimaNuova, value)
print ' prossima scadenza in escenic: %s - %s' % (prossimaFA, valueFA)

if not( prossimaNuova is None): 
	if  ( prossimaNuova != prossimaFA or value != valueFA ):
		Helpers.MandaProssimaFinestraEscenic(value, delta)

	Helpers.ScriviFA( value[0], value[1] )

Helpers.ScriviDb( finestre_per_escenic, begintime )



