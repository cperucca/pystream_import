import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil
import time
import json

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'vdf' : '{http://www.vizrt.com/types}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

############################################ configuration #################################################
# production :
livestream = [ 2651854]
link = "http://internal.publishing.rsi.ch/webservice/escenic/content/"
# staging : 
# livestream = [ 215113 ]
# link = "http://internal.publishing.staging.rsi.ch/webservice/escenic/content/"

############################################ configuration #################################################


def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl8811')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return


def Put_Link( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')
	file = open(filename)
	dati = file.read()

	request = urllib2.Request(link, data=dati)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('If-Match', '*')
	request.add_header('Content-Type', 'application/atom+xml')
	request.get_method = lambda: 'PUT'
	result = urllib2.urlopen(request)
	
	print result.read()
	return
	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return


def Delete_Link( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl8811')).replace('\n', '')

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('If-Match', '*')
	request.add_header('Content-Type', 'application/atom+xml')
	request.get_method = lambda: 'DELETE'
	result = urllib2.urlopen(request)
	
	print result.read()
	return
	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return

def GetPicture( link , filename):

	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	req = urllib2.urlopen(request)
	with open(filename, 'wb+') as fp:
	    shutil.copyfileobj(req, fp)
        return


def PutPicture( link , filename):

	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	req = urllib2.urlopen(request)
	with open(filename, 'wb+') as fp:
	    shutil.copyfileobj(req, fp)
        return

def Put_Picture( link, filename ):

	# TEST : non funziona

	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')
	file = open(filename)
	dati = file.read()

	request = urllib2.Request(link, data=dati)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('X-Escenic-media-filename', 'IMAGE_NAME')
	request.add_header('Content-Type', 'image/png')
	request.get_method = lambda: 'POST'
	result = urllib2.urlopen(request)
	
	return




#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/section/ROOT/subsections", 'subsections')
#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/section/4/content-items", 'content_items')
#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/open-search/escenic/4/content-search-description.xml", 'content_search')
#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/search/escenic/4/2216587/?pw=1&c=100", 'search')
# Dump_Link(link + '2651854', 'tvsvizzera_live_2651854')
#Put_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/215113", './tvsvizzera_live_12_30')
#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/215332", 'tvsvizzera_keyframe')
#GetPicture("http://internal.publishing.staging.rsi.ch/webservice/escenic/binary/215332/2014/9/4/13/128781_6.png", 'pino.png')
#PutPicture("http://internal.publishing.staging.rsi.ch/webservice/publication/tvsvizzera/binary/image", './pino.png')

#Delete_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/215360", 'keyframe_delete')


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			return lis.attrib['href']
	return None


def PrendiField( entry , rel):

	list =  entry.findall(namespaces['vdf'] + "field")

	for idx , lis in enumerate(list):
		print idx, lis
		print lis.attrib['name']
		if rel in lis.attrib['name']:
			return lis.find(namespaces['vdf'] + "value").text

	return None

# formatta lo starttime in un oggetto datetime
# comodo per eventuali confronti di data
def FormatDateTime( startTime, delta):
	#print startTime
	startDateTime = datetime.datetime.strptime( startTime, "%Y-%m-%dT%H:%M:%S.%fZ" )
	#print  startDateTime + delta

	return startDateTime + delta

# questa server per calcolare l'oggetto datetime di END
# prsso dallo start + duration
def CalcEndTime( startTime, duration):
	#print startTime , startTime + datetime.timedelta( seconds = int(duration))
	endTime = startTime + datetime.timedelta( milliseconds = int(duration))
	return endTime


def CalcDeltaTimeUtc():

	nowutc = datetime.datetime.utcnow()
	now = datetime.datetime.now()
	delta = now - nowutc
	#print delta
	
	deltahour = datetime.timedelta(hours=int(delta.seconds/3600))
	#2:00:02.320133
	#print deltahour

	return deltahour

def GetLiveStreaming( link, id ):
		
	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')

	link = link  + str(id)
	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	return result

def PutLiveStreaming( link, id, data ):

	base64string = base64.encodestring('%s:%s' % ('tvsvizzera_admin', 'admin')).replace('\n', '')
	print data

	#fin = codecs.open('./tvsvizzera_live_2651854', 'r')
	#data = fin.read()
	#fin.close()
	#print '\n'
	#print data

	link = link + str(id)
	request = urllib2.Request(link, data=data)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('If-Match', '*')
	request.add_header('Content-Type', 'application/atom+xml')
	request.get_method = lambda: 'PUT'
	result = urllib2.urlopen(request)
	print result.read()
	
	return




def Cambia_Definition( atom, data, dict_def ):
	
	lista_date = ["eventTime", "startTime", "endTime"]
	tree = ET.parse(atom)
	#tree = ET.fromstring(atom)
	#ET.dump(tree)

	# per cambiare data a lista_date = ["eventTime", "startTime", "endTime"]
	lista_fields = tree.findall( namespaces['atom'] + 'content' + '/' +  namespaces['vdf'] + 'payload' + '/' +  namespaces['vdf'] + 'field')
	print lista_fields
	
	# gestione del time control
	tree.find(namespaces['dcterms'] + 'available').text = dict_def['available'].replace('XX_TODAY_XX', data.strftime("%Y-%m-%d"))
	available = tree.find(namespaces['dcterms'] + 'available').text
	print available

	tree.find(namespaces['age'] + 'expires').text = dict_def['expires'].replace('XX_TODAY_XX', data.strftime("%Y-%m-%d"))
	expires = tree.find(namespaces['age'] + 'expires').text
	print expires
	
	for entry in lista_fields:

		name = entry.get('name')

		if name in lista_date:
			value = entry.find(namespaces['vdf'] + 'value').text
			print value
			entry.find(namespaces['vdf'] + 'value').text = dict_def[name].replace('XX_TODAY_XX', data.strftime("%Y-%m-%d"))
			value = entry.find(namespaces['vdf'] + 'value').text
			print name, value
		if name == 'title':
			entry.find(namespaces['vdf'] + 'value').text = dict_def['title']
			value = entry.find(namespaces['vdf'] + 'value').text
			print name, value
		if name == 'subtitle':
			entry.find(namespaces['vdf'] + 'value').text = dict_def['subtitle'].replace('XX_TODAY_XX', data.strftime("%d-%m-%Y"))
			value = entry.find(namespaces['vdf'] + 'value').text
			print name, value
		if name == 'stream':
			# devo andare dentro a prendere i due stream e cambiarli in accordo con quanto messo
			# nella lista degli stream nel dict_def
			streams = entry.findall(namespaces['vdf'] + 'list' + '/' + namespaces['vdf'] + 'payload')
			
			for counter, strea in enumerate(streams):
				print strea.find(namespaces['vdf'] + 'field' + '/' + namespaces['vdf'] + 'value').text
				print dict_def['stream'][counter]
				strea.find(namespaces['vdf'] + 'field' + '/' + namespaces['vdf'] + 'value').text = dict_def['stream'][counter]


	return tree

def Prendi_Definition( time ,dict ):

	# per testare 
	# time = '14:01'
	
	if dict.has_key( time ):
		return dict[time]

	return None

dict_def = {'00:01':{'id':'2651854','title':'Telegiornale 12.30','subtitle':'Non perdetevi la fascia informativa serale a partire dalle 19.00 e alle 20.00 il TG','eventTime':'XX_TODAY_XXT10:20:00Z','startTime':'XX_TODAY_XXT10:20:00Z','endTime':'XX_TODAY_XXT10:50:00Z', 'available':'XX_TODAY_XXT00:00:00Z', 'expires':'XX_TODAY_XXT21:59:00.00Z', 'stream':['http://srgssruni15ww-lh.akamaihd.net/z/enc15uni_ww@191861/manifest.f4m','https://srgssruni15ww-lh.akamaihd.net/i/enc15uni_ww@191861/master.m3u8']}, '14:01':{'id':'2651854','title':'Informazione serale','subtitle':'Edizione del XX_TODAY_XX - Ore 19.00 Il Quotidiano; ore 20.00 Il Telegiornale; ore 20.30 La Meteo','eventTime':'XX_TODAY_XXT16:55:00Z','startTime':'XX_TODAY_XXT16:55:00Z','endTime':'XX_TODAY_XXT18:50:00Z', 'available':'XX_TODAY_XXT00:00:00Z', 'expires':'XX_TODAY_XXT21:59:00.00Z', 'stream':['http://srgssruni15ww-lh.akamaihd.net/z/enc15uni_ww@191861/manifest.f4m','https://srgssruni15ww-lh.akamaihd.net/i/enc15uni_ww@191861/master.m3u8']}}

# prende la data attuale
date  = datetime.datetime.now()
#date = datetime.datetime( 2014, 07, 23, 19, 00, 00, 0)
#print date.strftime("%Y-%m-%d")


for liv in livestream:
	time = datetime.datetime.now().strftime("%H:%M")
	print time
	dict_definition = Prendi_Definition( time, dict_def )
	print dict_definition
	if dict_definition is None:
		print 'Esco perche non sono al momento giusto'
		exit(0)
	#for key,value in dict_definition.iteritems():
		#print key,value
	livestream_atom = GetLiveStreaming( link,  liv )	
	live_new_date = Cambia_Definition( livestream_atom, date , dict_definition )
	root = live_new_date.getroot()
	live_data = ET.tostring(root)
	dat =  '<?xml version="1.0"?>\n' + live_data
	PutLiveStreaming( link, liv, dat  )
	



