import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import shutil
import stat
import pickle
import pexpect

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'vdf' : '{http://www.vizrt.com/types}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }


Livestreaming_Dir = './'
Template_Dir = '/home/perucccl/pyStream_Import/TEMPLATES/'

_Home_Dir = '/var/www/html/WS/progr-live-streaming-tvs/'
_Db_Dir = _Home_Dir + '/Data/DB/'
_Resources_Dir = _Home_Dir + '/Resources/'


def replaceStringInFile(line_to_replace, dict, data):

   for key, value in dict.iteritems():
       #print key, value.replace('XX_TODAY_XX', data.strftime("%Y-%m-%d"))
       outtext=line_to_replace.replace(key,value.replace('XX_TODAY_XX', data.strftime("%Y-%m-%d")))
       line_to_replace=outtext

   return outtext

def CreateLiveStreamingXml( l_dictionary , data ):

        nomefile =  Livestreaming_Dir + '._livestreaming_template_.xml'
        print nomefile

        nometemplate = Template_Dir + l_dictionary['TEMPLATE_FILE'] 

        fxml = codecs.open( nomefile, 'w+', 'utf-8')
        ftempl = codecs.open( nometemplate, 'r', 'utf-8')

        for line in ftempl:
                fxml.write( replaceStringInFile( line, l_dictionary, data ))


        ftempl.close()
        fxml.close()
        return



def Dump_Link ( link, filename , base64string):

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('claudio.perucca@rsi.ch', 'SxMQDeir')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('post@adactus.no', 'V1m2e3o4#')).replace('\n', '')



	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	#print result.read()

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	exit(0)

	return



def Dump_Rows ( link, filename , base64string):

	print ' in Dump Rows '

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	#base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib2.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			request = urllib2.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib2.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return
		
def CalcDeltaTimeUtc():

	nowutc = datetime.datetime.utcnow()
	now = datetime.datetime.now()
	delta = now - nowutc
	#print delta
	
	deltahour = datetime.timedelta(hours=int(delta.seconds/3600))
	#2:00:02.320133
	#print deltahour

	return deltahour


def PrendiProssimaScadenza(lista, time):

		
	for value in lista:
		#print value
		if time < value[0] :
			return [ value[0], value ]
		if time < value[1] :
			return [ value[1], value ]
	 
	return[ None, None ]

def doScp(user,password, host, path, fNames):
	print fNames
	child = pexpect.spawn('scp %s %s@%s:%s' % (fNames, user, host,path))
	print 'scp %s %s@%s:%s' % (fNames, user, host,path)
	i = child.expect(['assword:', r"yes/no"], timeout=30)
	if i == 0:
	    child.sendline(password)
	elif i == 1:
	    child.sendline("yes")
	    child.expect("assword:", timeout=30)
	    child.sendline(password)
	data = child.read()
	print data
	child.close()


def MandaProssimaFinestraEscenic(lista, delta, activate, expire):


        # calcolo i valori di start e end dalla lista
        tmp_start = lista[0] - delta
        tmp_end = lista[1] - delta
        # modifica da Paolo del 10-09-2014 per inserire anche eventtime
        # e per mettere event a -2 dalla partenza
        # e start a -4 dalla partenza
        due = datetime.timedelta( seconds=120 )
        event = tmp_start - due
        tmp_start = event - due
        start = tmp_start.strftime("%Y-%m-%dT%H:%M:%SZ")
        end = tmp_end.strftime("%Y-%m-%dT%H:%M:%SZ")
        event = event.strftime("%Y-%m-%dT%H:%M:%SZ")

	print 'mando a escenic start %s - end %s ' % ( start, end)
	# devo mandar la roba ad escenic
	to_escenic_import_dir = '/var/escenic/import/default/tvsvizzera'
	#to_escenic_import_dir = '/tmp'
	# posso farlo con la curl oppure 
	# alla solita maniera con la import
	
	fout = codecs.open( _Db_Dir + '_clad_tmp_', 'w', 'utf-8')
	# prendo il temnplate
	#nometemplate = Template_dir + '/Resource_Template.xml'
	nometemplate =  _Resources_Dir + 'Resource_Template.xml'
        ftempl = codecs.open( nometemplate, 'r', 'utf-8')
	for line in ftempl:
		# nel mentre che lo scrivo fuori 
		# gli cambio XX_START_XX con lista[0]
		# e XX_END_XX con lista[1]
		fout.write(line.replace('XX_START_XX' , start).replace('XX_END_XX', end).replace('XX_EVENT_XX', event).replace('XX_AVAILABLE_XX',activate).replace('XX_EXPIRES_XX',expire))

	ftempl.close()
	fout.close()

	# quindi lo muovo nella diretory di import 
	# di escenic
	#shutil.copy(WorkDir + '/_clad_tmp_', to_escenic_import_dir)
	# shutil.copy(_Db_Dir + '_clad_tmp_', to_escenic_import_dir + '/stream_update.xml')
	doScp( 'perucccl', 'WaWA3pu', 'rsis-zp-ecpub3', to_escenic_import_dir+ '/stream_update.xml', _Db_Dir + '_clad_tmp_')

	#gli cambio le permissions cosi la import 
	#puo leggerlo senza problemi 
	#st = os.stat( to_escenic_import_dir + '/stream_update.xml')
	#os.chmod( to_escenic_import_dir + '/stream_update.xml', st.st_mode | stat.S_IROTH | stat.S_IWOTH)
	
	
	#os.remove(WorkDir + '/_clad_tmp_')
	os.remove(_Db_Dir + '_clad_tmp_')
		
def PrendiProssimaScadenza(lista, time):

		
	for value in lista:
		#print value
		if time < value[0] :
			return [ value[0], value ]
		if time < value[1] :
			return [ value[1], value ]
	 
	return[ None, None ]




def CheckLimits(time, begintime, endtime ):
	if time < begintime or time > endtime:
		# devo chiudere se e' aperto
		stato_rubinetto = PrendiStatoRubinetto()
		if stato_rubinetto:
			# se e' acceso lo spengo
			#while the command:
			#http://10.72.10.27/rest/digitalItem/withdraw/742?publishPoint=3
			#will turn off the live Stream for RSI La Uno
			print ' Spengo il rubinetto '
			stato_rubinetto = False
			out = 'OFF'
			fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'w' )
			fin.write(out)
			fin.close()
			exit(0)
		else:
			# sono fuori dai limiti e il rubinetto e' chiuso
			# tutto a posto : esco e sto tranquillo
			exit(0)
			
def ScriviFA( start, end ):

	# adesso devo scrivere se sono acceso o spento
	# per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	# io a modificare lo stato
	fout = open(_Db_Dir + "._fa_stream_la1_tvsvizzera_.", 'w' )
	fout.write(start.strftime("%Y-%m-%dT%H:%M:%SZ" ))
	fout.write(',')
	fout.write(end.strftime("%Y-%m-%dT%H:%M:%SZ" ))
	fout.write('\n')
	fout.close()


def PrendiFA():

	# adesso devo guardare se ero acceso o spento
	#per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	#io a modificare lo stato
	fin = open(_Db_Dir + "._fa_stream_la1_tvsvizzera_.", 'r' )
	db_in = fin.read().splitlines()
	fin.close()

	fa = []
	print ' la finestra attuale e  : '
	for value in db_in:
		#print value
		start =  datetime.datetime.strptime( value.split(',')[0] ,"%Y-%m-%dT%H:%M:%SZ")  
		end =  datetime.datetime.strptime( value.split(',')[-1] ,"%Y-%m-%dT%H:%M:%SZ")  
		fa.append( [ start, end ]  )


	for fin in fa:
		print fin

	return fa

def ScriviStatoRubinetto( value ):

	# adesso devo scrivere se sono acceso o spento
	# per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	# io a modificare lo stato
	fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'w' )
	fin.write(value)
	fin.close()


def PrendiStatoRubinetto():

	# adesso devo guardare se ero acceso o spento
	#per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	#io a modificare lo stato
	fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'r' )
	stato_rubinetto = fin.read().splitlines()
	fin.close()

	print stato_rubinetto

	if 'ON' in stato_rubinetto:
		stato_rubinetto = True
	else:
		stato_rubinetto = False

	return stato_rubinetto

def PrendiPaolinoDb( time ):
        # apri il db delle finestre
        # da mettere sotto lock del file .stream_la1_tvsvizzera.lock.

        date = time.strftime("%Y-%m-%d")

        print date
        #aspetto sul lock
        while os.path.isfile(_Db_Dir + '/.paolino_tvsvizzera.lock.') :
                print 'aspetto che finisca di lavorare il crea liste '
        print ' cerco il file :' +  _Db_Dir + '/._' + date + '_paolino_tvsvizzera_.'
        if not os.path.exists(_Db_Dir + '/._' + date + '_paolino_tvsvizzera_.'):
                print ' non ho trovato il Db per la data ' + date
                return None

        # prendo il lock
        open(_Db_Dir + '/.paolino_tvsvizzera.lock.', 'a').close()

        # apro il db
        fin = codecs.open(_Db_Dir + '/._' + date + '_paolino_tvsvizzera_.', 'r', 'utf-8')
        # leggo le finestre
        db_in = fin.read().splitlines()
        fin.close()
        # e rilascio il lock
        os.remove(_Db_Dir + '/.paolino_tvsvizzera.lock.')

        return db_in


def PrendiDb( time ):
	# apri il db delle finestre
	# da mettere sotto lock del file .stream_la1_tvsvizzera.lock.
	date = time.strftime("%Y-%m-%d")

	#aspetto sul lock
	while os.path.isfile('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.') : 
		print 'aspetto che finisca di lavorare il crea liste '

	if not os.path.exists("/home/perucccl/pyStream/._" + date + "_stream_la1_tvsvizzera_."):
		return None

	# prendo il lock
	open('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.', 'a').close()

	# apro il db
	fin = codecs.open("/home/perucccl/pyStream/._" + date + "_stream_la1_tvsvizzera_.", 'r', 'utf-8')
	# leggo le finestre
	db_in = fin.read().splitlines()
	fin.close()
	# e rilascio il lock
	os.remove('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.')

	db_finestre = []
	print ' il db conteneva  era : '
	for value in db_in:
		#print value
		start =  datetime.datetime.strptime( value.split(',')[0] ,"%Y-%m-%dT%H:%M:%SZ")  
		end =  datetime.datetime.strptime( value.split(',')[-1] ,"%Y-%m-%dT%H:%M:%SZ")  
		db_finestre.append( [ start, end ]  )


	for fin in db_finestre:
		print fin

	return db_finestre

def ScriviDb( louise_dict , time ):

	#aspetto sul lock
	while os.path.isfile('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.') : 
		print 'aspetto che finisca di lavorare il rubinetto '
	# prendo il lock
	open(_Db_Dir + '.stream_la1_tvsvizzera.lock.', 'a').close()
	# e adesso salvala sul db delle finestre
	date =time.strftime("%Y-%m-%d")
	fout = codecs.open(_Db_Dir + "._" + date + "_stream_la1_tvsvizzera_.", 'w', 'utf-8')
	for lis in louise_dict:
		#print lis[0]
		fout.write(lis[0].strftime("%Y-%m-%dT%H:%M:%SZ" ))
		fout.write(',')
		fout.write(lis[1].strftime("%Y-%m-%dT%H:%M:%SZ" ))
		fout.write('\n')
	fout.close()
	# e rilascio il lock
	os.remove(_Db_Dir + '.stream_la1_tvsvizzera.lock.')



def ScriviDict( dict , time, tipo ):

	#print time.strftime("%Y-%m-%dT%H:%M:%SZ" )
	timestamp =  time.strftime("%Y-%m-%d_%H_%M_%S_%f" )
	fout = open('/home/perucccl/pyStream/RESOURCES/' + tipo + '_' + timestamp + '_db', 'wb')

	pickle.dump( dict, fout )
	fout.close()

