import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil

DEBUG = False

base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

def PrendiField( entry , rel):

	list =  entry.findall(Helpers.namespaces['vdf'] + "field")

	for idx , lis in enumerate(list):
		#if (DEBUG): print idx, lis
		#if (DEBUG): print lis.attrib['name']
		if rel == lis.attrib['name']:
			return lis.find(Helpers.namespaces['vdf'] + "value").text

	return None

# formatta lo starttime in un oggetto datetime
# comodo per eventuali confronti di data
def FormatDateTime( startTime, delta):
	#if (DEBUG): print startTime
	startDateTime = datetime.datetime.strptime( startTime, "%Y-%m-%dT%H:%M:%S.%fZ" )
	#if (DEBUG): print  startDateTime + delta

	return startDateTime + datetime.timedelta(days=0)

# questa serve per calcolare l'oggetto datetime di END
# prsso dallo start + duration
def CalcEndTime( startTime, duration):
	#if (DEBUG): print startTime , startTime + datetime.timedelta( seconds = int(duration))
	endTime = startTime + datetime.timedelta( milliseconds = int(duration))
	return endTime


# con questa mi calcolo la differenza dal fuso di Greenwitch
# e la sbatto nella globalona delta che sommo all'inizio agli
# start delle liste e poi lavoro sempre in naive mode
delta = Helpers.CalcDeltaTimeUtc()
#if (DEBUG): print delta

def TogliDiritti( dict_entry ):

	if (DEBUG): print 'Togli Diritti'
	lista_finestre_senza_diritti = []

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	count = 0
	Senza_Diritti = False

	# 0 title 1
	# 1 title 2
	# 2 starttime
	# 3 duration
	# 4 endtime
	# 5 geolimited
	# 6 href a i metadata
	# 7 start offset
	# 8 vme asset id
	# 9 startbroadcast
	# 10 endvroadcast 
	
	for key, value in sorted_dict:
		if (DEBUG): print value[2], value[4], value[5]
		if not value[5]: 
			# il 5 est True significa che non abbiamo i diritti
			# o perche i metadati non ci sono oppure perche il geolimited e true
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti mode allora allungo la finestra
				if (DEBUG): print ' allungo la finestra '
				date = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				
				#lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date + delta ]
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date + datetime.timedelta(days=0) ]
				
			else :
				# altrimenti inizio una finestra chiusa finche dura'
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				end = datetime.datetime.strptime( value[10], "%Y-%m-%dT%H:%M:%SZ" )
				if (DEBUG): print 'inizio finestra chiusa a %s ' % (date + datetime.timedelta(days=0))
				#lista_finestre_senza_diritti.append(  [ date + delta, end + delta ] )
				lista_finestre_senza_diritti.append(  [ date + datetime.timedelta(days=0), end + datetime.timedelta(days=0) ] )
			Senza_Diritti = True
			
		else:
			#se invece la entry ha i diritti
			if Senza_Diritti:
				# se ero gia' in Senza_Diritti  mode allora chiudo la 
				# finestra di chiusura che inizia uno spazio buono
				date = datetime.datetime.strptime( value[9], "%Y-%m-%dT%H:%M:%SZ" )
				if (DEBUG): print ' fine la finestra chiusa a %s ' % (date+ datetime.timedelta(days=0))
				
				#lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ delta ]
				lista_finestre_senza_diritti[ count ] = [ lista_finestre_senza_diritti[count][0] , date+ datetime.timedelta(days=0) ]
				count = count + 1

				
			else :
				# altrimenti allungo lo spazio buono
				if (DEBUG): print ' allungo lo spazio buono '
			Senza_Diritti = False
			

	return lista_finestre_senza_diritti

def ExtractRange( dict_entry, begintime, endtime):

	dict_estratto = {}

	sorted_dict = sorted(dict_entry.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		if begintime <= value[2] <= endtime or begintime <= value[4] <= endtime:
			dict_estratto[key] = value
			#if (DEBUG): print key, value
	

	return dict_estratto

def  CreaListaApriChiudi(louise, veda):

	lista_ac = {}


	sorted_dict = sorted(louise.items(), key=lambda i: i[1][2])
	sorted_dict_veda = sorted(veda.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		if (DEBUG): print value[0], value[8]
		if (DEBUG): print ' trovato - %s - in louise con id : %s ' % ( value[0], value[8] )
		if (DEBUG): print ' comincia alle : %s e finisce alle: %s ' % (value[2], value[4])
		if (DEBUG): print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])

	if (DEBUG): print '\n\n'
	for key, value in sorted_dict:
		if (DEBUG): print ' comincia alle : %s e finisce alle: %s ' % (value[9], value[10])


	return lista_ac

def Prendi_Begintime( dict_in ):
	sorted_dict = sorted(dict_in.items(), key=lambda i: i[1][2])
	for key, value in sorted_dict:
		return value[2]
	return 'None'

def Merge_Negative( lista_in , begintime):

	
	if (DEBUG): print ' Merge _Negative '
	s1 = begintime - datetime.timedelta(seconds=1)
	domani = datetime.datetime.now() + datetime.timedelta(days=1)
	s2  = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0) -delta
	filter = [[s1,s2]]
	if (DEBUG): print 'Filter : '
	if (DEBUG): print filter
	# eventuale lista di test
	#m1 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 05, 35, 00, 0) - delta 
	#m2 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 12, 30, 00, 0) - delta
#
	#m3 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 12, 42, 00, 0) - delta
	#m4 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 19, 00, 00, 0) - delta
#
	#m5 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 20, 39, 00, 0) - delta 
	#m6 = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0) -delta

#
	#m7 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 16, 07, 00, 0) - delta
	#m8 = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 17, 10, 00, 0) - delta
	#l1  = [[m1,m2], [m3,m4], [m5,m6] ]


	#times = lista_in + filter
	times = lista_in 
	#times = l1 + filter

	lista_result = Merge( sorted(times) )

	return lista_result

def Merge( times ):
	
	if (DEBUG): print ' Times 1'
	for tim in times:
		if (DEBUG): print tim
	if (DEBUG): print ' Times 2'
	saved = [times[0]]
	if (DEBUG): print ' Saved '
	if (DEBUG): print saved
	if (DEBUG): print ' Saved '
	for st, end in times[1:]:
		if (DEBUG): print saved[-1]
		if (DEBUG): print st, end
		a = saved[-1][0]
		if (DEBUG): print a
		b = saved[-1][1]
		if (DEBUG): print b
		
		if st <= b < end:
			saved[-1] = a,end
		elif b<st<=end:
			saved.append([st,end])
		else:
			pass
		if (DEBUG): print ' Saved '
		if (DEBUG): print saved
		if (DEBUG): print ' Saved '
	return saved

def TrimmaFinestre(lista, begintime, endtime):

	if (DEBUG): print ' TrimmaFinestre '
	# entrano finestre negative
	if (DEBUG): print begintime
	if (DEBUG): print endtime
	# sistema i limiti estremi
	for lis in lista:
		if (DEBUG): print lis[0], lis[1]
		if lis[0] < begintime: 
			#if (DEBUG): print  str(lis[0]) + ' minore di ' + str(begintime)
			lis[0] = begintime
		if lis[1] > endtime : 
			#if (DEBUG): print  str(lis[1]) + ' maggiore di ' + str(endtime)
			lis[1] = endtime
	
	# aggiunta 02 10 2014 per permettere l inserimento dei limiti
	# editoriali dalle 12:25 alle 12:50 
	# e dalle 18_55 alle 20_40
	# che corrispondono a due finestre chiuse dalle 12:00 alle 12:25
	# e dalle 12:50 alle 18:55
	
	for lin in lista:
		if (DEBUG): print lin

	if (DEBUG): print 'Fine  TrimmaFinestre '
	lista = Merge_Negative( lista, begintime )
	
	return lista


def Negate_Finestre( lista_f_c, begintime, endtime ):
	
	if (DEBUG): print '\n Inizia Negate Finestre ...\n '
	# lista finestre aperte per adesso vuota
	lista_fa = []
	
	#if (DEBUG): print lista_f_c[0][0]
	# la riempio con la finestra totale di apertura
	# da begin time a endtime
	lista_fa.append([ begintime, endtime]) 

	count = 0
	for lin in lista_f_c:
		if (DEBUG): print ' start %s - end %s ' % (lin[0], lin[1])
		lista_fa[count] = [ lista_fa[count][0], lin[0] ]
		lista_fa.append( [ lin[1], lin[1]]  )
		if (DEBUG): print lista_fa
		count = count + 1
		
	lista_fa[len(lista_fa)-1] = [ lista_fa[len(lista_fa)-1][0], endtime ]

	if (DEBUG): print ' \nfinale Finestre CON DIRITTI ' 
	#if (DEBUG): print lista_fa
	
	result_fa = []

	# toglie le finestre che hanno inizio == fine
	for fa in lista_fa:
		if (DEBUG): print ' start %s - end %s ' % (fa[0], fa[1])
		if not fa[0] == fa[1]:
			result_fa.append(fa)
	if (DEBUG): print ' \nfinale  Finestre CON DIRITTI trimmata in orario nostro:' 
	for fa in result_fa:
		if (DEBUG): print ' start %s - end %s ' % (fa[0] + delta, fa[1] + delta)
	if (DEBUG): print '\n'
	if (DEBUG): print '\n Fine Negate Finestre ...\n '
	if (DEBUG): print ' \nfinale  Finestre CON DIRITTI trimmata senza delta:' 
	for fa in result_fa:
		if (DEBUG): print ' start %s - end %s ' % (fa[0], fa[1])
	if (DEBUG): print '\n'
	if (DEBUG): print '\n Fine Negate Finestre ...\n '
			
	return result_fa	

def Paolino_2_Dict(paolino_db, time):


	if (DEBUG): print ' Paolino_2_Dict'
	result_dict = {}

	for idx, lis in enumerate(paolino_db):
		lis =  lis.split('_!_')
		if (DEBUG): print lis
		#start = datetime.datetime( time.year, time.month, time.day, int(lis[1].split(':')[0]), int(lis[1].split(':')[1]),int(lis[1].split(':')[-1]),0)
		start =  datetime.datetime.strptime( lis[1], "%Y-%m-%d %H:%M:%S" )
		startF = start.strftime("%Y-%m-%dT%H:%M:%SZ")
		end = datetime.datetime.strptime( lis[2], "%Y-%m-%d %H:%M:%S" )
		endF = end.strftime("%Y-%m-%dT%H:%M:%SZ")
		Off_uguale_False = False if 'Off' in str(lis[4]) else True
		result_dict[ lis[0] + '_' + str(idx) ] = [lis[0], lis[0] ,start, 'dummy', end, Off_uguale_False, 'dummy',  'dummy',  'dummy',  startF,  endF,   ]

	if (DEBUG): print ' Fine  Paolino_2_Dict'
	return result_dict

begintime = datetime.datetime( datetime.datetime.now().year,  datetime.datetime.now().month,  datetime.datetime.now().day, 01, 00, 00, 0)
begintime = begintime - delta
activate = begintime.strftime("%Y-%m-%d %H:%M:%S.0")

domani = datetime.datetime.now() + datetime.timedelta(days=1)

endtime = datetime.datetime( domani.year,  domani.month,  domani.day, 00, 59, 00, 0)
endtime = endtime - delta
expire = endtime.strftime("%Y-%m-%d %H:%M:%S.0")
if (DEBUG): print ' begin Time = %s ' % begintime
if (DEBUG): print ' end Time = %s ' % endtime


 
time = datetime.datetime.now()
#time = datetime.datetime( 2014,07,12, 19, 50, 01, 0) 

paolino_db = Helpers.PrendiPaolinoDb( time ) 

louise_dict = Paolino_2_Dict(paolino_db, time) 


#sorted_dict = sorted(louise_dict.items(), key=lambda i: i[1][2])
#for key, value in sorted_dict:
	#if (DEBUG): print key,value

begintime = Prendi_Begintime( louise_dict )

if (DEBUG): print ' NEW begin Time = %s ' % begintime

louise_dict = TogliDiritti( louise_dict )
if (DEBUG): print '\n  Louise : finestre SENZA diritti  # %d ' % len(louise_dict)

louise_dict = TrimmaFinestre(louise_dict, begintime, endtime)

if (DEBUG): print ' \n Finestre SENZA diritti : '
for lis in louise_dict:
	if (DEBUG): print ' start %s - end %s ' % (lis[0], lis[1])

finestre_per_escenic = Negate_Finestre( louise_dict, begintime, endtime )
for lis in finestre_per_escenic:
	if (DEBUG): print lis

db_finestre = Helpers.PrendiDb (begintime)
# se mi ha tornato None significa che per oggi nessuno
# ha ancora fatto il Db continuiamo tranquilli

if not db_finestre is None:
	if (DEBUG): print ' esisteva db '

# adesso posso guardare le cose che sono cambiate
# e scrivere il log per Paolo .. TODO

# adesso prima di riscriverlo fuori 
# in base a tempo e prossima apertura/chiusura
# dobbiamo mandare le giuste cose a escenic


# prendo il valore della FA finestra Attuale
fa = Helpers.PrendiFA()

[prossimaNuova, value] = Helpers.PrendiProssimaScadenza(finestre_per_escenic, time)
[prossimaFA, valueFA] = Helpers.PrendiProssimaScadenza(fa, time)

if (DEBUG): print ' prossima scadenza delle nuove: %s - %s' % (prossimaNuova, value)
if (DEBUG): print ' prossima scadenza in escenic: %s - %s' % (prossimaFA, valueFA)

if not( prossimaNuova is None): 
	if  ( prossimaNuova != prossimaFA or value != valueFA ):
		if (DEBUG): print ' Helpers.MandaProssimaFinestraEscenic'
		if (DEBUG): print 'Value : ' + str(value)
		if (DEBUG): print prossimaNuova
		if (DEBUG): print prossimaFA
		if (DEBUG): print ' valueFA : ' + str(valueFA)
		Helpers.MandaProssimaFinestraEscenic(value, datetime.timedelta(hours=1), activate, expire)

	#if (DEBUG): print 'Value sotto: ' + str(value)
	Helpers.ScriviFA( value[0], value[1] )

Helpers.ScriviDb( finestre_per_escenic, begintime )



